from django.urls import path

from dashboard.views import index_dashboard, crear_producto_dashboard, borrar_producto_dashboard, \
    editar_producto_dashboard

app_name = 'dashboard'

urlpatterns = [
    path('', index_dashboard, name='index'),
    path('crear-producto/', crear_producto_dashboard, name='crear_producto'),
    path('borar-producto/<int:pk>/', borrar_producto_dashboard, name='borrar_producto'),
    path('editar-producto/<int:pk>/', editar_producto_dashboard, name='editar_producto'),

]
