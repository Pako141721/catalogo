from django import forms
from productos.models import Producto


class CrearProductoForm(forms.Form):
    nombre = forms.CharField()
    imagen = forms.ImageField()


class CrearProductoModelForm(forms.ModelForm):
    class Meta:
        model = Producto
        fields='__all__'
        #fields = ['nombre', 'imagen']
