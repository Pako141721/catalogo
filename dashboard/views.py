from django.shortcuts import render, redirect

from productos.models import Producto
from .forms import CrearProductoModelForm


# Create your views here.

def index_dashboard(request):
    objects_list = Producto.objects.all()
    context = {
        'objects_list': objects_list
    }
    return render(request, 'dashboard/index.html', context)


def crear_producto_dashboard(request):
    form = CrearProductoModelForm
    if request.method == 'POST':
        form = CrearProductoModelForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('dashboard:index')
    return render(request, 'dashboard/crear_producto.html', {'form': form, 'titulo': 'Nuevo Producto'})


def borrar_producto_dashboard(request, pk):
    Producto.objects.get(pk=pk).delete()
    return redirect('dashboard:index')


def editar_producto_dashboard(request, pk):
    producto = Producto.objects.get(pk=pk)
    form = CrearProductoModelForm(instance=producto)
    if request.method == 'POST':
        form = CrearProductoModelForm(request.POST, request.FILES, instance=producto)
        if form.is_valid():
            form.save()
        return redirect('dashboard:index')
    return render(request, 'dashboard/crear_producto.html', {'form': form, 'titulo': 'Actuliazar Producto'})
