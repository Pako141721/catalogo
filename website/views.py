from django.shortcuts import render

from productos.models import Producto


def index_website(request):
    objects_list = Producto.objects.all()
    context = {
        'objects_list': objects_list
    }

    return render(request, 'website/index.html', context)


def detalle_producto(request, pk):
    producto = Producto.objects.get(pk=pk)
    return render(request, 'website/detalle_producto.html', {'producto': producto})
