from django.urls import path

from .views import index_website, detalle_producto

app_name = 'website'

urlpatterns = [
    path('', index_website, name='index_website'),
    path('detalle-producto/<int:pk>/', detalle_producto, name='detalle_producto'),

]
