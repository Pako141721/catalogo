from django.contrib import admin

from productos.models import Producto, ImagenesProducto

# Register your models here.

admin.site.register(Producto)
admin.site.register(ImagenesProducto)
